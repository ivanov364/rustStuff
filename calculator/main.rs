use std::env::{args, Args};

fn main() {
    let mut args: Args = args();

    let first_arg = args.nth(1).unwrap();
    let operator = args.nth(0).unwrap().chars().next().unwrap();
    let second_arg = args.nth(0).unwrap();

    let x = first_arg.parse::<f32>().unwrap();
    let y = second_arg.parse::<f32>().unwrap();
    let result = operators(operator, x, y);

    println!("{:?}", output(x, operator, y, result));
}

fn operators(operator: char, x: f32, y: f32) -> f32{
    match operator {
        '+' => x+y,
        '-' => x-y,
        '*' | 'x' | 'X' => x*y,
        '/' => x/y,
        _ => panic!("Invalid operator used.")
    }
}

fn output(x: f32, operator: char, y: f32, result: f32) -> String{
    format!("{} {} {} = {}", x, operator, y, result)
}
